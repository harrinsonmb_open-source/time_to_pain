<?php

declare(strict_types=1);

namespace Tests\Domain\Worklog;

use App\Domain\Worklog\SimplifiedWorklog;
use JsonException;
use Tests\TestCase;

class WorklogTest extends TestCase
{
    public function worklogProvider(): array
    {
        return [
            [1, ['name' => 'harrinsonmb'], '2h 30m', 9000, '2021-08-02T08:00:00.000Z','2021-08-02T08:00:00.000Z'],
            [2, ['name' => 'johndoe'], '2h 30m', 9000, '2021-08-02T08:00:00.000Z', '2021-08-02T08:00:00.000Z'],
            [3, ['name' => 'martinfiz'], '2h 30m', 9000, '2021-08-02T08:00:00.000Z', '2021-08-02T08:00:00.000Z'],
            [4, ['name' => 'arnordcm'], '2h 30m', 9000, '2021-08-02T08:00:00.000Z', '2021-08-02T08:00:00.000Z'],
            [5, ['name' => 'harrinsonmb'], '2h 30m', 9000,'2021-08-02T08:00:00.000Z', '2021-08-02T08:00:00.000Z']
        ];
    }

    /**
     * @dataProvider worklogProvider
     * @param int $id
     * @param array $author
     * @param string $timeSpent
     * @param int $timeSpentSeconds
     * @param string $started
     * @param string $updated
     */
    public function testGetters(
        int $id,
        array $author,
        string $timeSpent,
        int $timeSpentSeconds,
        string $started,
        string $updated
    ): void
    {
        $worklog = new SimplifiedWorklog($id, $author, $timeSpent, $timeSpentSeconds, $started, $updated);

        $this->assertEquals($id, $worklog->getId());
        $this->assertEquals($author['name'], $worklog->getAuthorName());
        $this->assertEquals($timeSpent, $worklog->getTimeSpent());
        $this->assertEquals($timeSpentSeconds, $worklog->getTimeSpentSeconds());
        $this->assertEquals($started, $worklog->getStarted());
        $this->assertEquals($updated, $worklog->getUpdated());
    }

    /**
     * @dataProvider worklogProvider
     * @param int $id
     * @param array $author
     * @param string $timeSpent
     * @param int $timeSpentSeconds
     * @param string $started
     * @param string $updated
     * @throws JsonException
     */
    public function testJsonSerialize(
        int $id,
        array $author,
        string $timeSpent,
        int $timeSpentSeconds,
        string $started,
        string $updated
    ): void
    {
        $worklog = new SimplifiedWorklog($id, $author, $timeSpent, $timeSpentSeconds, $started, $updated);

        $expectedPayload = json_encode([
            'id' => $id,
            'authorName' => $author['name'],
            'timeSpent' => $timeSpent,
            'timeSpentSeconds' => $timeSpentSeconds,
            'started' => $started,
            'updated' => $updated,
        ], JSON_THROW_ON_ERROR);

        $this->assertEquals($expectedPayload, json_encode($worklog, JSON_THROW_ON_ERROR));
    }
}

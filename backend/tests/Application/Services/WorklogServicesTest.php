<?php

namespace Tests\Application\Services;

use App\Application\Services\WorklogService;
use App\Domain\Worklog\SimplifiedWorklog;
use Exception;
use JiraRestApi\Issue\Issue;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueSearchResult;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Issue\IssueType;
use JiraRestApi\Issue\PaginatedWorklog;
use JiraRestApi\Issue\Worklog;
use JiraRestApi\JiraException;
use JiraRestApi\Project\Project;
use JsonMapper_Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use stdClass;

class WorklogServicesTest extends TestCase
{
    private SimplifiedWorklog $mockedSimplifiedWorklog;
    private WorklogService $worklogService;
    private IssueService $issueService;

    private Issue $mockedIssue;

    private LoggerInterface $logger;

    private PaginatedWorklog $mockedPaginatedWorklog;

    private Worklog $mockedWorklog;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        // Crear un mock de Logger
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->issueService = $this->createMock(IssueService::class);
        $this->mockedSimplifiedWorklog = $this->createMock(SimplifiedWorklog::class);
        $this->mockedIssue = $this->createMock(Issue::class);
        $this->worklogService = new WorklogService($this->issueService, $this->logger);
        $this->mockedIssueSearchResult = $this->createMock(IssueSearchResult::class);
        $this->mockedPaginatedWorklog = $this->createMock(PaginatedWorklog::class);
        $this->mockedWorklog = $this->createMock(Worklog::class);
    }

    public function testQueryByJQLReturnsIssues_queryIssuesByJQLShouldReturnArrayOfIssues(): void
    {
        $jql = '';
        $this->mockedIssueSearchResult->expects($this->once())
            ->method('getIssues')
            ->willReturn([$this->mockedIssue]);

        $this->issueService->expects($this->once())
            ->method('search')
            ->with($jql)
            ->willReturn($this->mockedIssueSearchResult);

        $issues = $this->worklogService->queryIssuesByJQL($jql);

        $this->assertIsArray($issues);
        $this->assertNotEmpty($issues);
        $this->assertInstanceOf(Issue::class, $issues[0]);
    }

    public function testQueryByJQLThrowsJiraException_queryIssuesByJQLShouldReturnEmptyArray(): void
    {
        $jql = '';
        $this->mockedIssueSearchResult->expects($this->once())
            ->method('getIssues')
            ->willThrowException(new JiraException(''));

        $this->issueService->expects($this->once())
            ->method('search')
            ->with($jql)
            ->willReturn($this->mockedIssueSearchResult);

        // We expect the logger to be called once
        $this->logger->expects($this->once())
            ->method('error');

        $issues = $this->worklogService->queryIssuesByJQL($jql);

        // We expect the returned value to be an empty array
        $this->assertIsArray($issues);
        $this->assertEmpty($issues);
    }

    public function testQueryByJQLThrowsJsonMapper_Exception_queryIssuesByJQLShouldReturnEmptyArray(): void
    {
        $jql = '';
        $this->mockedIssueSearchResult->expects($this->once())
            ->method('getIssues')
            ->willThrowException(new JsonMapper_Exception(''));

        $this->issueService->expects($this->once())
            ->method('search')
            ->with($jql)
            ->willReturn($this->mockedIssueSearchResult);

        // We expect the logger to be called once
        $this->logger->expects($this->once())
            ->method('error');

        $issues = $this->worklogService->queryIssuesByJQL($jql);

        // We expect the returned value to be an empty array
        $this->assertIsArray($issues);
        $this->assertEmpty($issues);
    }

    public function testGetWorkLogsForIssuesReturnsWorklogs_getWorkLogsForIssuesShouldReturnArrayOfSimplifiedWorklogs(): void
    {
        $issue = $this->mockedIssue;
        $issue->key = '1';
        $issue->fields = new IssueField();
        $issue->fields->issuetype = new IssueType();
        $issue->fields->issuetype->name = 'issuetypeName';
        $issue->fields->project = new Project();
        $issue->fields->project->key = 'projectKey';

        $this->mockedWorklog->id = '1';
        $this->mockedWorklog->author = [
            "name" => "authorName",
        ];
        $this->mockedWorklog->key = '1';
        $this->mockedWorklog->timeSpent = 'timeSpent';
        $this->mockedWorklog->timeSpentSeconds = 'timeSpentSeconds';
        $this->mockedWorklog->started = 'started';
        $this->mockedWorklog->updated = 'updated';

        $this->mockedPaginatedWorklog->expects($this->once())
            ->method('getWorklogs')
            ->willReturn([$this->mockedWorklog]);

        $this->issueService->expects($this->once())
            ->method('getWorklog')
            ->with($issue->key)
            ->willReturn($this->mockedPaginatedWorklog);

        $worklogs = $this->worklogService->getWorkLogsForIssues([$issue]);

        $this->assertIsArray($worklogs);
        $this->assertNotEmpty($worklogs);
        $this->assertInstanceOf(SimplifiedWorklog::class, $worklogs[0]);
    }

    public function testGetWorkLogsForIssuesThrowsJiraException_getSimplifiedWorklogsForIssueShouldReturnEmptyArray(): void
    {
        $issue = $this->createMock(Issue::class);
        $issue->key = 'issueKey';

        $this->issueService->expects($this->once())
            ->method('getWorklog')
            ->with($issue->key)
            ->willThrowException(new JiraException(''));

        $worklogs = $this->worklogService->getSimplifiedWorklogsForIssue($issue);

        $this->assertIsArray($worklogs);
        $this->assertEmpty($worklogs);
    }
}

<?php

use DI\ContainerBuilder;
use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\Issue\IssueService;

return static function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        IssueService::class => function () {
            $jiraHost = getenv('JIRA_HOST');
            $jiraUser = getenv('JIRA_USER');
            $jiraPassword = getenv('JIRA_PASSWORD');

            return new IssueService(new ArrayConfiguration([
                'jiraHost' => $jiraHost,
                'jiraUser' => $jiraUser,
                'jiraPassword' => $jiraPassword
            ]));
        }
    ]);
};

<?php

namespace App\Application\Services;

use App\Domain\JQLQuery\JQLQuery;
use App\Domain\Worklog\SimplifiedWorklog;
use App\Domain\Worklog\WorklogGroupedIssueKey;
use DateTime;
use Exception;
use JiraRestApi\Issue\Issue;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Issue\Worklog;
use JiraRestApi\JiraException;
use JsonMapper_Exception;
use Psr\Log\LoggerInterface;

class WorklogService
{
    private LoggerInterface $logger;

    public IssueService $issueService;

    /**
     * @throws Exception
     */
    public function __construct(IssueService $issueService, LoggerInterface $logger)
    {
        $this->issueService = $issueService;
        $this->logger = $logger;
    }

    /**
     * @return Issue[]
     */
    public function queryIssuesByJQL(string $jql, int $startAt = 0, int $maxResults = 10): array
    {
        try {
            return $this->issueService->search($jql, $startAt, $maxResults)->getIssues();
        } catch (JiraException $e) {
            $this->logger->error("Error querying issues by JQL", [$e->getMessage(), $e->getCode()]);
            return [];
        } catch (JsonMapper_Exception $e) {
            $this->logger->error("Error mapping issues by JQL", [$e->getMessage(), $e->getCode()]);
            return [];
        }
    }

    /**
     * @param Issue[] $issues
     * @return SimplifiedWorklog[]
     */
    public function getWorkLogsForIssues(array $issues): array
    {
        $worklogsGroupedByIssue = array_map([$this, 'getSimplifiedWorklogsForIssue'], $issues);
        return array_merge(...$worklogsGroupedByIssue);
    }

    /**
     * @param Issue $issue
     * @return SimplifiedWorklog[]
     */
    public function getSimplifiedWorklogsForIssue(Issue $issue): array
    {
        $simplifiedWorklogs = [];
        try {
            $worklogs = $this->issueService->getWorklog($issue->key)->getWorklogs();
            $simplifiedWorklogs = array_map(fn($worklog) => $this->mapToSimplifiedWorklog($issue, $worklog), $worklogs);
        } catch (JiraException $e) {
            $this->logger->error("Error getting worklogs for issue", [$e->getMessage(), $e->getCode()]);
        } catch (JsonMapper_Exception $e) {
            $this->logger->error("Error mapping worklogs for issue", [$e->getMessage(), $e->getCode()]);
        }
        return $simplifiedWorklogs;
    }

    /**
     * @param SimplifiedWorklog[] $worklogs
     * @param JQLQuery $jqlQuery
     * @return SimplifiedWorklog[]
     * @throws Exception
     */
    public function filterWorklogsForIssuesWith(array $worklogs, JQLQuery $jqlQuery): array
    {
        $worklogs = array_filter($worklogs, function ($worklog) use ($jqlQuery) {
            $worklogAuthors = $jqlQuery->getWorklogAuthors();
            if (!empty($worklogAuthors)) {
                $authorsMatches = in_array($worklog->getAuthorName(), $worklogAuthors);
            } else {
                $authorsMatches = true;
            }

            $startDateWorklog = new DateTime($worklog->getStarted());

            $startDateQueried = new DateTime($jqlQuery->getWorklogStartDate());

            $endDateQueried = new DateTime($jqlQuery->getWorklogEndDate());
            // We set the last hour of the endDate
            $endDateQueried->modify('+1 day');

            return $authorsMatches && $this->isDateBetween($startDateWorklog, $startDateQueried, $endDateQueried);
        });

        return $worklogs;
    }

    /**
     * @param DateTime $queriedDate
     * @param DateTime|null $startDate
     * @param DateTime|null $endDate
     * @return bool
     */
    public function isDateBetween(DateTime $queriedDate, DateTime $startDate = null, DateTime $endDate = null): bool
    {
        $startDateMatches = false;
        $endDateMatches = false;

        if ($startDate === null || $queriedDate >= $startDate) {
            $startDateMatches = true;
        }

        if ($endDate === null || $queriedDate < $endDate) {
            $endDateMatches = true;
        }

        return $startDateMatches && $endDateMatches;
    }

    /**
     * @param Issue $issue
     * @param Worklog $worklog
     * @return SimplifiedWorklog
     */
    public function mapToSimplifiedWorklog(Issue $issue, Worklog $worklog): SimplifiedWorklog
    {
        $simplifiedWorklog = new SimplifiedWorklog();
        $simplifiedWorklog->setId($worklog->id);
        $simplifiedWorklog->setAuthorName($worklog->author['name']);
        $simplifiedWorklog->setIssueKey($issue->key);
        $simplifiedWorklog->setIssueType($issue->fields->issuetype->name);
        $simplifiedWorklog->setProjectKey($issue->fields->project->key);
        $simplifiedWorklog->setTimeSpent($worklog->timeSpent);
        $simplifiedWorklog->setTimeSpentSeconds($worklog->timeSpentSeconds);
        $simplifiedWorklog->setStarted($worklog->started);
        $simplifiedWorklog->setUpdated($worklog->updated);
        return $simplifiedWorklog;
    }

    /**
     * @param SimplifiedWorklog[] $filteredWorklogs
     * @return WorklogGroupedIssueKey[]
     */
    public function groupWorklogsByIssueKey(array $filteredWorklogs): array
    {
        // We create a map with issueKey => worklog in order to sum all the logs and set a WorklogGroupedIssueKey
        $reduce_result = array_reduce($filteredWorklogs, static function (array $carry, SimplifiedWorklog $item) {
            $issueKey = $item->getIssueKey();
            $timeSpentSeconds = $item->getTimeSpentSeconds();

            if (!isset($carry[$issueKey])) {
                $groupedWorklog = new WorklogGroupedIssueKey();
                $groupedWorklog->setAuthorName($item->getAuthorName());
                $groupedWorklog->setIssueKey($item->getIssueKey());
                $groupedWorklog->setIssueType($item->getIssueType());
                $groupedWorklog->setProjectKey($item->getProjectKey());
                $groupedWorklog->setStarted($item->getStarted());
                $groupedWorklog->setUpdated($item->getUpdated());
                $groupedWorklog->setTimeSpent("");
                $groupedWorklog->setTimeSpentSeconds(0);

                $carry[$issueKey] = $groupedWorklog;
            } else {
                $groupedWorklog = $carry[$issueKey];
            }

            $groupedWorklog->addTimeSpentInSeconds($timeSpentSeconds);

            return $carry;
        }, []);

        // We return only the WorklogGroupedIssueKey values
        return array_values($reduce_result);
    }

    /**
     * @param int $sec
     * @return string
     */
    public static function convertSecToTime(int $sec): string
    {
        $date1 = new DateTime("@0");
        $date2 = new DateTime("@$sec");
        $interval = $date1->diff($date2);

        $parts = ['years' => 'y', 'months' => 'm', 'days' => 'd',
            'hours' => 'h', 'minutes' => 'i', 'seconds' => 's'];
        $formatted = [];
        foreach ($parts as $i => $part) {
            $value = $interval->$part;
            if ($value !== 0) {
                if ($value === 1) {
                    $i = substr($i, 0, -1);
                }
                $formatted[] = "$value $i";
            }
        }

        if (count($formatted) === 1) {
            return $formatted[0];
        }

        $str = implode(', ', array_slice($formatted, 0, -1));
        $str .= ' and ' . $formatted[count($formatted) - 1];
        return $str;
    }

}

<?php

namespace App\Application\Actions\Worklogs;

use App\Domain\JQLQuery\JQLQuery;
use Exception;
use Psr\Http\Message\ResponseInterface as Response;

class ListWorklogsAction extends WorklogAction
{
    /**
     * @throws Exception
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();

        $authorNames = $queryParams['authorNames'] ?? null;
        $startDate = $queryParams['startDate'] ?? null;
        $endDate = $queryParams['endDate'] ?? null;

        $jqlQuery = new JQLQuery($authorNames, $startDate, $endDate);

        $jql = $jqlQuery->parseParametersIntoJQL();

        $this->logger->info(sprintf("jql => %s", $jql));

        $issues = $this->worklogService->queryIssuesByJQL($jql);

        $simplifiedWorklogsForIssues = $this->worklogService->getWorkLogsForIssues($issues);

        $filteredWorklogs = $this->worklogService->filterWorklogsForIssuesWith($simplifiedWorklogsForIssues, $jqlQuery);

        $groupedWorlogs = $this->worklogService->groupWorklogsByIssueKey($filteredWorklogs);

        return $this->respondWithData($groupedWorlogs);
    }
}

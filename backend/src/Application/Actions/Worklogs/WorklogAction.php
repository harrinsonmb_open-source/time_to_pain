<?php

namespace App\Application\Actions\Worklogs;

use App\Application\Actions\Action;
use App\Application\Services\WorklogService;
use Psr\Log\LoggerInterface;

abstract class WorklogAction extends Action
{
    protected WorklogService $worklogService;

    public function __construct(LoggerInterface $logger, WorklogService $worklogService)
    {
        parent::__construct($logger);
        $this->worklogService = $worklogService;
    }
}

<?php

namespace App\Domain\Worklog;

use App\Application\Services\WorklogService;

class WorklogGroupedIssueKey extends AbstractWorklog
{
    public function addTimeSpentInSeconds(int $seconds): void
    {
        $this->timeSpentSeconds += $seconds;
        $this->timeSpent = WorklogService::convertSecToTime($this->timeSpentSeconds);
    }
}

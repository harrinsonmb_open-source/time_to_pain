<?php

declare(strict_types=1);

namespace App\Domain\Worklog;

use JsonSerializable;

class SimplifiedWorklog extends AbstractWorklog
{
    private int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}

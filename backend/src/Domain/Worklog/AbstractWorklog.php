<?php

namespace App\Domain\Worklog;

abstract class AbstractWorklog implements \JsonSerializable
{
    protected string $authorName;

    protected string $issueKey;

    protected string $issueType;

    protected string $projectKey;

    protected string $timeSpent;

    protected int $timeSpentSeconds;

    protected string $started;

    protected string $updated;

    public function getAuthorName(): string
    {
        return $this->authorName;
    }

    public function setAuthorName(string $authorName): void
    {
        $this->authorName = $authorName;
    }

    public function getIssueKey(): string
    {
        return $this->issueKey;
    }

    public function setIssueKey(string $issueKey): void
    {
        $this->issueKey = $issueKey;
    }

    public function getIssueType(): string
    {
        return $this->issueType;
    }

    public function setIssueType(string $issueType): void
    {
        $this->issueType = $issueType;
    }

    public function getProjectKey(): string
    {
        return $this->projectKey;
    }

    public function setProjectKey(string $projectKey): void
    {
        $this->projectKey = $projectKey;
    }

    public function getTimeSpent(): string
    {
        return $this->timeSpent;
    }

    public function setTimeSpent(string $timeSpent): void
    {
        $this->timeSpent = $timeSpent;
    }

    public function getTimeSpentSeconds(): int
    {
        return $this->timeSpentSeconds;
    }

    public function setTimeSpentSeconds(int $timeSpentSeconds): void
    {
        $this->timeSpentSeconds = $timeSpentSeconds;
    }

    public function getStarted(): string
    {
        return $this->started;
    }

    public function setStarted(string $started): void
    {
        $this->started = $started;
    }

    public function getUpdated(): string
    {
        return $this->updated;
    }

    public function setUpdated(string $updated): void
    {
        $this->updated = $updated;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return array_filter(get_object_vars($this));
    }
}

<?php

namespace App\Domain\JQLQuery;

class JQLQuery
{
    /**
     * @var string[]
     */
    private array $worklogAuthors;
    private string $worklogStartDate;
    private string $worklogEndDate;

    /**
     * @return string[]
     */
    public function getWorklogAuthors(): array
    {
        return $this->worklogAuthors;
    }

    public function getWorklogStartDate(): string
    {
        return $this->worklogStartDate;
    }

    public function getWorklogEndDate(): string
    {
        return $this->worklogEndDate;
    }

    public function __construct($worklogAuthors, $worklogStartDate, $worklogEndDate)
    {
        $this -> worklogAuthors = $worklogAuthors;
        $this -> worklogStartDate = $worklogStartDate;
        $this -> worklogEndDate = $worklogEndDate;
    }

    public function parseParametersIntoJQL(): string
    {
        $sentences = [];
        if (!empty($this->worklogAuthors)) {
            $sentences[] = sprintf("worklogAuthor in (%s)", implode(", ", $this->worklogAuthors));
        }

        if (!empty($this->worklogStartDate)) {
            $sentences[] = sprintf("worklogDate >= %s", $this->worklogStartDate);
        }

        if (!empty($this->worklogEndDate)) {
            $sentences[] = sprintf("worklogDate <= %s", $this->worklogEndDate);
        }

        return implode(" AND ", $sentences);
    }
}

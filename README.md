## Rutas/servicios principales
* / ⇒ Redirección a la app
* /app ⇒ Ruta principal de la aplicación (React)
* /docs ⇒ Ruta principal de la documentación de la API (Swagger)
* /api ⇒ Ruta principal de la API (PHP)
## Frontend React
## Frontend queries
La idea es tener un formulario que te permita hacer consultas al backend para obtener los worklogs y mostrar las tareas en las que se ha imputado.
### Campo de fechas
Podemos empezar por un campo de texto donde le pasamos una fecha en un formato concreto y en un futuro, que sea un datepicker. En realidad serían dos campos de fechas, la fecha de inicio de la búsqueda y la fecha de fin.
### Campo de usuarios
Empezaremos por poner un campo de texto donde podremos pasar un nombre de usuario para la búsqueda y en un futuro, un selector multiple con búsqueda de usuarios autocompletada.
### inputs de paginación
Añadir paginador para no mostrar ni pedir toda la información de golpe
## Frontend filters
### Filtro de proyectos
Para empezar podríamos poner un grupo de checkboxes que permita filtrar worklogs con los proyectos disponibles
### Filtro de usuarios
Podríamos trabajar un filtro que permita ocultar o mostrar los worklogs de los usuarios que queramos
## Frontend views
### Vista de tabla
Las columnas pueden ser las siguientes:
- Autor
- Proyecto
- Tarea y link a ella
- Descripción
- Horas empleadas
- Estado
- Tipo de tarea
## API REST con PHP (Slim framework)
### Servicios necesarios del Jira client
#### UserService
Se usa para obtener la información de los `users` de la consulta
#### IssueService
Se usa para obtener la información de los `issues`. Con él obtendremos las tareas de jira que tienen imputado el tiempo y también las imputaciones relacionadas.
## FAQ
Es posible que el servidor de jira esté en una vpn y que el docker no sea capaz de acceder a la url. En ese caso aplica las soluciones que se comentan en https://stackoverflow.com/questions/33904647/docker-container-and-host-network-vpn, es posible que haya conflictos entre las ips de docker y de la vpn, con esto se puede solucionar.
## TODO
- Página con las últimas imputaciones del usuario actual
